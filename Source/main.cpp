//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

int main (int argc, const char* argv[])
{
    
    int myFunction(int counter);
    
    
    int num;
    
    std::cout << "Enter a number" << std::endl;
    
    std::cin >> num;
    
    num = myFunction(num);
    
    std::cout << "Num = " << num;
    
    
    return 0;
}


int myFunction(int counter)
{
    int num = 0;
    
    if(counter <=1) return 1;
    
      num = counter * myFunction(counter-1);
   
    return num;
    
}
